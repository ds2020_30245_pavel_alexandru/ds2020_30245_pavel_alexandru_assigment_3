package hessian;

import net.minidev.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

public interface DispenserInterface {

    ArrayList<JSONObject> getAllMedicationPlans(UUID id);

}
