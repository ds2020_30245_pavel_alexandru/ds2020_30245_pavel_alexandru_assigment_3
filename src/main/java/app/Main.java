package app;

import hessian.DispenserInterface;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class Main {


    @Bean
    public HessianProxyFactoryBean hessianInvoker() {

        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        invoker.setServiceUrl("https://pavel-alexandru-a3-backend.herokuapp.com/dispenser");
        invoker.setServiceInterface(DispenserInterface.class);
        return invoker;

    }
    public static void main(String[] args) {

        DispenserInterface dispenserInterface = SpringApplication.run(Main.class, args).getBean(DispenserInterface.class);
        System.setProperty("java.awt.headless", "false");
        Gui gui = new Gui(dispenserInterface);
    }
}
