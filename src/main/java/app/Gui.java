package app;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hessian.DispenserInterface;
import net.minidev.json.JSONObject;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Gui {

    JFrame frame;
    JPanel panel;
    JLabel buna,time;
    JTable table;
    JScrollPane jScrollPane;
    DispenserInterface dispenserInterface;
    LocalDateTime time1;

    void timeThread()
    {
        Thread timer = new Thread(() ->
        {
            while (true) {

                time1 = java.time.LocalDateTime.now();
                time.setText(time1.getHour() + ":" + time1.getMinute() + ":" + time1.getSecond());

                //SETAM ORA DE DOWNLOAD PT MEDICATION PLAN-uri
                if (time1.getHour() == 22 && time1.getMinute() == 00 && time1.getSecond() == 00)
                {
                    ArrayList<JSONObject> medicationPlan = dispenserInterface.getAllMedicationPlans(UUID.fromString("0886d5c6-b54a-43ac-a358-91e06a71582c"));
                    table=getTable(medicationPlan);
                    panel.remove(jScrollPane);
                    jScrollPane = new JScrollPane(table);
                    jScrollPane.setPreferredSize(new Dimension(900,400));
                    jScrollPane.setForeground(new Color(0, 153, 153));
                    panel.add(jScrollPane);

                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    int counter=1;
                    for(JSONObject o : medicationPlan)
                    {
                        Date dateS = null;
                        try {
                            dateS = new SimpleDateFormat("yyyy-MM-dd").parse(o.getAsString("startDate"));

                        Date dateE = new SimpleDateFormat("yyyy-MM-dd").parse(o.getAsString("endDate"));

                        LocalDate date = LocalDate.now();
                        Date date1 = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());

                        if(date1.after(dateS) && date1.before(dateE)) {

                            String json = gson.toJson(o);
                            System.out.println("Medication plan " + counter + "\n" + json + "\n");
                            counter++;

                        }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        timer.start();
    }

    public JTable getTable(ArrayList<JSONObject> medicationPlan)
    {
        JTable table;
        String[] columns = {"Medication name", "Intake interval"};
        ArrayList<String> meds = new ArrayList<>();
        ArrayList<String> medsTime = new ArrayList<>();

        for (JSONObject o : medicationPlan)
        {
            try {
                Date dateS = new SimpleDateFormat("yyyy-MM-dd").parse(o.getAsString("startDate"));
                Date dateE = new SimpleDateFormat("yyyy-MM-dd").parse(o.getAsString("endDate"));

            LocalDate date = LocalDate.now();
            Date date1 = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());

            if(date1.after(dateS) && date1.before(dateE))
            {
                String[] medsSplit = o.getAsString("meds").split(",");
                String[] medsTimeSplit = o.getAsString("medsTime").split(",");
                for (int i = 0; i < medsSplit.length; i++) {
                    meds.add(medsSplit[i]);
                    medsTime.add(medsTimeSplit[i]);
                }
            }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Object[][] tableInfo = new Object[meds.size()][2];
        for (int i = 0; i < meds.size(); i++)
        {
            tableInfo[i][0] = meds.get(i);
            tableInfo[i][1] = medsTime.get(i);
        }
        DefaultTableModel defaultTableModel = new DefaultTableModel(tableInfo, columns);
        table = new JTable(defaultTableModel);
        return table;
    }

    public Gui(DispenserInterface dispenserInterface) {

        this.dispenserInterface = dispenserInterface;
        ArrayList<JSONObject> medicationPlan = dispenserInterface.getAllMedicationPlans(UUID.fromString("0886d5c6-b54a-43ac-a358-91e06a71582c"));

        frame = new JFrame("Pill Dispenser");
        frame.setSize(1000,700);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setBackground(new Color(93, 166, 163));
        frame.getContentPane().add(panel);

        buna = new JLabel("Pill Dispenser",SwingConstants.CENTER);
        buna.setForeground(new Color(92, 196, 224));
        buna.setFont(new Font("Arial", Font.BOLD, 80));
        buna.setPreferredSize(new Dimension(1000,100));
        panel.add(buna);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        time = new JLabel(LocalDateTime.now().format(formatter),SwingConstants.CENTER);
        time.setForeground(new Color(92, 196, 224));
        time.setFont(new Font("Arial", Font.BOLD, 40));
        time.setPreferredSize(new Dimension(1000,50));
        panel.add(time);

        table=getTable(medicationPlan);
        jScrollPane = new JScrollPane(table);
        jScrollPane.setPreferredSize(new Dimension(900,400));
        jScrollPane.setForeground(new Color(0, 153, 153));
        panel.add(jScrollPane);

        timeThread();
    }

}
